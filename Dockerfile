# Use Alpine as the base image
FROM alpine:latest

# Set the timezone environment variable
ENV TZ=America/Detroit

# Create a directory for the application
RUN mkdir -p /srv/app/JMusicBot
WORKDIR /srv/app/JMusicBot

# Accept the variables as build-time arguments
ARG TOKEN
ARG OWNER
ARG VERSION

# Copy the configuration template file
COPY config.txt /srv/app/JMusicBot/config.txt

# Use sed to replace placeholders with actual values from arguments
RUN sed -i "s/{TOKEN}/$TOKEN/g" config.txt
RUN sed -i "s/{OWNER}/$OWNER/g" config.txt

# Update the package list, install necessary packages, and clean up
# Install OpenJDK Java Runtime Environment (Alpine uses 'apk' for package management)
RUN apk add --no-cache wget openjdk11-jre-headless && \
    # Setting up timezone data
    apk add --no-cache tzdata && \
    cp /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    apk del tzdata

# Download the JMusicBot application
RUN wget https://github.com/jagrosh/MusicBot/releases/download/${VERSION}/JMusicBot-${VERSION}.jar -O /srv/app/JMusicBot/JMusicBot.jar

# Set the entry point to run the Java application
ENTRYPOINT ["java", "-Dnogui=true", "-jar", "/srv/app/JMusicBot/JMusicBot.jar"]
